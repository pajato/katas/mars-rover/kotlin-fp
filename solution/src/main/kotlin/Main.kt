package com.pajato.katas.marsrover.fp

typealias Point = Pair<Int, Int>

fun execute(
    input: String,
    position: Point = Point(0, 0),
    direction: Char = 'N',
    obstacles: List<Point> = listOf()
): String {
    tailrec fun recurse(index: Int, position: Point, direction: Char): String {
        fun nextDirection(move: Char) = when {
            (direction == 'N' && move == 'R') || (direction == 'S' && move == 'L') -> 'E'
            (direction == 'E' && move == 'R') || (direction == 'W' && move == 'L') -> 'S'
            (direction == 'S' && move == 'R') || (direction == 'N' && move == 'L') -> 'W'
            else -> 'N'
        }
        fun nextX(x: Int) = when (direction) {
            'W' -> if (x == 0) 9 else x - 1
            'E' -> if (x == 9) 0 else x + 1
            else -> x
        }
        fun nextY(y: Int) = when (direction) {
            'N' -> if (y == 9) 0 else y + 1
            'S' -> if (y == 0) 9 else y - 1
            else -> y
        }
        fun hasObstacle(action: Char): Boolean {
            val nextPosition = Pair(nextX(position.first), nextY(position.second))
            return action == 'M' && obstacles.contains(nextPosition)
        }
        val (x, y) = position

        return when {
            index == input.length -> "$x:$y:$direction"
            hasObstacle(input[index]) -> "O:${position.first}:${position.second}:$direction"
            else -> when (input[index]) {
                'R' -> recurse(index + 1, position, nextDirection('R'))
                'L' -> recurse(index + 1, position, nextDirection('L'))
                'M' -> recurse(index + 1, Pair(nextX(x), nextY(y)), direction)
                else -> recurse(index + 1, position, direction)
            }
        }
    }

    return recurse(0, position, direction)
}