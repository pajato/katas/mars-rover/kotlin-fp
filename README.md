# kotlin-fp

A functional programming style implementation of the [Mars Rover kata](https://katalyst.codurance.com/mars-rover) using Kotlin.

In this implementation we start with the following assumptions:

1) A single Mars Rover robot is placed on a 10x10 grid with a function named execute() calculating the position and
direction for the rover after a sequence of moves.

2) The 10x10 grid is addressed by x:y where 0,0 is the bottom, left position; x increments to the east, y increments to
the north and both wrap at the grid edge.

3) The implementation consists of one top level function with the signature:

    fun execute(
        input: String,
        postion: Pair<Int, Int> = Pair(0, 0)
        direction: Char = 'N',
        obstacles: List<Pair<Int, Int>> = listOf()
    ) -> String

where "input" is a string representing the possible moves: M (move one step in the current direction), L (turn to face
left) , R (turn to face right) with all other characters being ignored. "position" is the initial x, y coordinates for
the rover on the grid. "obstacles" is a list of positions where movement is halted. If no obstacles are encountered, the
output is a formatted string showing the current position and direction in the form x:y:direction, e.g. "3:7:E". When an
obstacle is encountered the output is of the form 0:x:y:direction, e.g. "0:1:3:W" where "x" and "y" are the position
where the move action "M" started.
